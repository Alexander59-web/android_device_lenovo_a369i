# Inherit device configuration
$(call inherit-product, device/lenovo/a369i/device.mk)

# Device identifier. This must come after all inclusions
PRODUCT_DEVICE := a369i
PRODUCT_NAME := omni_a369i
PRODUCT_BRAND := lenovo
PRODUCT_MANUFACTURER := lenovo
PRODUCT_MODEL := lenovo_a369i
