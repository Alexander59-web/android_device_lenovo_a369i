TWRP device tree for Lenovo A369i (rev.1, rev.2 and rev.lite), lenovo a319 with custom kernel and custom partitions
========================================================

to change the device, run the script
a319 - change_a319.sh
a369i_rev.1 - change_rev1.sh
a369i_rev.2 - change_rev2.sh
a369i_rev.lite - change_rev_lite.sh
For building TWRP for mt6572 models only.
